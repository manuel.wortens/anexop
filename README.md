# anexop

Anexo P de Python de la tesis doctoral "Hacia el etiquetado de Estados Informativos: experimentación en notas periodísticas del español del noroeste de México" en donde se integran algunos archivos de Python utilizados para la investigación, además que una sección de COPENOR tratada.

Por lo pronto, se puede acceder a la base de datos en este enlace:
[GOOGLE DRIVE _ ANEXO P](https://drive.google.com/drive/folders/1FS5tF0c2EqJXpQuM3Br0pE2CAB__LWje?usp=sharing)
